/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */

/*module.exports = {
  plugins: [
    `gatsby-plugin-fontawesome-css`
  ],
  siteMetadata : {
    title:'Web Warrior',
    description:'Web Dev Portfolio',
    copyright:'This website is copyright 2022 web warrior'
  },
}*/

module.exports = {
  /* Your site config here */
  plugins: [
    `gatsby-plugin-fontawesome-css`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `notes`,
        path: `${__dirname}/src/notes/`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `projects`,
        path: `${__dirname}/src/projects/`,
      },
    },
  ],
  siteMetadata: {
    title: 'Hackan Programmer',
    description: 'Learn Web Development',
    copyright: 'This website is copyright 2021 Hackan Programmer'
  },
 
}
