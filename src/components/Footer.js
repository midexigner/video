import React from 'react'
import {Link} from 'gatsby';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faYoutube,
  faTwitter,
  faInstagram,
  faGithub,
} from '@fortawesome/free-brands-svg-icons'

const Footer = () => {
  return (
    <section className='social bg-primary' id='social'>
        <div className='container'>
        <h3>Follow Hackan Programmer</h3>
        <div>
        <Link to='/'><FontAwesomeIcon icon={faYoutube} size="2x" /></Link>
        <Link to='/'><FontAwesomeIcon icon={faTwitter} size="2x" /></Link>
        <Link to='/'><FontAwesomeIcon icon={faInstagram} size="2x" /></Link>
        <Link to='/'><FontAwesomeIcon icon={faGithub} size="2x" /></Link>
        </div>
        </div>
        </section>
  )
}

export default Footer