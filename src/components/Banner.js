import {Link,graphql,useStaticQuery} from 'gatsby';
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faCogs,
  faGraduationCap,
} from '@fortawesome/free-solid-svg-icons'
import {
  faYoutube
} from '@fortawesome/free-brands-svg-icons'

const Banner = () => {
  const data = useStaticQuery(graphql`
 {
  site {
    id
    siteMetadata {
      title
      description
      copyright
    }
  }
}
`)
const {title} = data.site.siteMetadata;
  return (
    <section className='banner__section'>
              <div className='container'>
              <header>
                    <h5>{title}</h5>
                    <h1>
            Web Development Courses For 
            <span className="text-primary">Everyone</span>
          </h1>
          <p class="lead">
            Practical project-based courses that are easy to understand and
            straight to the point with NO FLUFF
          </p>
                   <Link to='/'> <button className='btn btn-primary'>Watch Now</button></Link>
                   <div className='stats'>
                     <div className='stat'>
                     <FontAwesomeIcon icon={faCogs} size="4x" />
                     <h3>15</h3>
                     <p>Years of Experience</p>
                     </div>
                     <div className='stat'>
                     <FontAwesomeIcon icon={faGraduationCap} size="4x" />
                     <h3>400K+</h3>
                     <p>Course Students</p>
                     </div>
                     <div className='stat'>
                     <FontAwesomeIcon icon={faYoutube} size="4x" />
                     <h3>15</h3>
                     <p>Years of Experience</p>
                     </div>
                   
                   </div>
                </header>
                </div>
            </section>
  )
}

export default Banner