import React from 'react'
import {Link} from 'gatsby';

const Header = () => {
  return (
    <nav className="navbar">
    <div className="container">
      <Link href="/">
        <img src="logo.png" alt="logo" />
      </Link>
      <ul className="desktop-menu">
        <li><Link to="#">Courses</Link></li>
        <li><Link to="#">About</Link></li>
        <li><Link to="#">Testimonials</Link></li>
        <li><Link to="#">YouTube</Link></li>
      </ul>
    </div>
  </nav>
  )
}

export default Header