import React from 'react'
import {Link} from 'gatsby';

const Course = () => {
  return (
    <section className="courses bg-dark-pattern">
    <div className="container">
    <h2><span className="text-primary">Latest </span>Courses</h2>
    <p className="text-center">
          Hover over the course to see this months discount code
          <div className="promo"></div>
        </p>
        <div className="course-grid">
        <div class="card">
            <img
              src="react_front_to_back_small.png"
              alt=""
              class="course-img"
            />
            <div class="card-body">
              <div class="level">Beginner - Intermediate</div>
              <h3>React Front To Back 2022</h3>
            </div>
            <div class="card-footer">
              <img src="udemy-logo.png" alt="" />
              <Link to="" target="_blank">View On Udemy</Link>
              <div class="promo-code">CODE: <span class='promo'>REACTF2B<span class="promo-display"></span></span></div>
            </div>
          </div>
          <div class="card">
            <img
              src="50projects_small.png"
              alt=""
              class="course-img"
            />
            <div class="card-body">
              <div class="level">Beginner</div>
              <h3>50 Projects in 50 Days - HTML, CSS & JavaScript</h3>
            </div>
            <div class="card-footer">
              <img src="udemy-logo.png" alt="" />
              <Link to="" target="_blank">View On Udemy</Link>
              <div class="promo-code">CODE: <span class='promo'>50PROJECTS<span class="promo-display"></span></span></div>
            </div>
          </div>

          <div class="card">
            <img
              src="next_small.png"
              alt=""
              class="course-img"
            />
            <div class="card-body">
              <div class="level">Intermediate</div>
              <h3>Next.js Dev To Deployment</h3>
            </div>
            <div class="card-footer">
              <img src="udemy-logo.png" alt="" />
              <Link to="" target="_blank">View On Udemy</Link>
              <div class="promo-code">CODE: <span class='promo'>NEXT<span class="promo-display"></span></span></div>
            </div>
          </div>
        </div>
        <h2><span class="text-primary">Popular</span> Courses</h2>
        <div className="course-grid">
        <div class="card">
            <img
              src="html_small.png"
              alt=""
              class="course-img"
            />
            <div class="card-body">
              <div class="level">Beginner</div>
              <h3>Modern HTML & CSS From The Beginning</h3>
            </div>
            <div class="card-footer">
              <img src="udemy-logo.png" alt="" />
              <Link to="" target="_blank">View On Udemy</Link>
              <div class="promo-code">CODE: <span class='promo'>HTMLCSS<span class="promo-display"></span></span></div>
            </div>
          </div>
          <div class="card">
            <img
              src="js_small.png"
              alt=""
              class="course-img"
            />
            <div class="card-body">
              <div class="level">Beginner</div>
              <h3>Modern JavaScript From The Beginning</h3>
            </div>
            <div class="card-footer">
              <img src="udemy-logo.png" alt="" />
              <Link to="" target="_blank">View On Udemy</Link>
              <div class="promo-code">CODE: <span class='promo'>JS<span class="promo-display"></span></span></div>
            </div>
          </div>

          <div class="card">
            <img
              src="js20_small.png"
              alt=""
              class="course-img"
            />
            <div class="card-body">
              <div class="level">Beginner</div>
              <h3>20 Vanilla JavaScript Projects</h3>
            </div>
            <div class="card-footer">
              <img src="./images/udemy-logo.png" alt="" />
              <Link to="" target="_blank">View On Udemy</Link>
              <div class="promo-code">CODE: <span class='promo'>20PROJECTS<span class="promo-display"></span></span></div>
            </div>
          </div>
        </div>
        <button class="btn btn-secondary btn-more">Show more courses</button>
    </div>
  </section>
  )
}

export default Course