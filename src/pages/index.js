import React from "react"
import Banner from "../components/Banner"
import Course from "../components/Course"
import Layout from "../components/Layout"
/* import { withPrefix } from 'gatsby';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faPlayCircle
} from '@fortawesome/free-solid-svg-icons' */

export default function Home() {
 
  return (
  <Layout>
  <Banner />
  <section id="email" class="email bg-primary">
      <div class="container">
        <h3>Notify Me Of New Courses</h3>
        <form class="ml-block-form" action="https://static.mailerlite.com/webforms/submit/t4b3n6"  method="post" >
          <input type="email" class="email form-control" name="fields[email]" placeholder="Email" autocomplete="email" required="" />
          <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="btn btn-dark">Notify Me</button>

        </form>
        </div>
      
    </section>
  <Course />
  </Layout>

    )
}


/*export const query = graphql`
query SiteInfo {
  site {
    id
    siteMetadata {
      title
      description
      copyright
    }
  }
}
`;*/
